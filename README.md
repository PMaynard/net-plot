# Net Plot

This program listens for TCP connections which contain a 4 byte value. The value is converted from scientific notation into a (.6) float. Then added to a rrd file for graphing.

![10min.png example](example10min.png)

## Requirements

- go
- rrd
- librrd-dev

# Deploy as a user service

Place the service file in the correct place:

	mkdir -p   ~/.config/systemd/user
	cp netplot.service ~/.config/systemd/user/

Enable, start, then check the status of the new service:

	systemctl --user enable netplot.service
	systemctl --user start netplot.service
	systemctl --user status netplot.service	

If you modify the service file, you need to update systemd:

	systemctl --user daemon-reload

# RRD

## Create the rrd file

Creates a new rrd file to store the records in. The incremental step is set to 5 seconds. 

	rrdtool create out.rrd --step 5 DS:temp1:GAUGE:600:12:50 RRA:MAX:0.5:1:2016

## Create graph

Creates a chart for the last 10mins. See the file `generateCharts.sh` for other options.

	rrdtool graph 10min.png -w 785 -h 120 -a PNG --start -600 -e now DEF:temp1=temp.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"

# Souce and License

Source code maintained on GitLab: <https://gitlab.com/PMaynard/netplot.git>

MIT Copyright 2019 Peter Maynard

#/bin/bash

# Creats a number of charts, then copies them over to a web site to be displayed. 
# Once it has done that, the script will sleep for 1 minute and do it again forever.

for (( ; ; ))
do 
	echo "Generating charts"
	rrdtool graph 10min.png -w 785 -h 120 -a PNG --start -600 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	rrdtool graph 30min.png -w 785 -h 120 -a PNG --start -1800 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	rrdtool graph 3hr.png -w 785 -h 120 -a PNG --start -10800 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	rrdtool graph 2d.png -w 785 -h 120 -a PNG --start -172800 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	rrdtool graph 5d.png -w 785 -h 120 -a PNG --start -432000 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	rrdtool graph 3m.png -w 785 -h 120 -a PNG --start -7889400 -e now DEF:temp1=out.rrd:temp1:MAX LINE1:temp1#ff0000:"Core (°C)"
	echo "Copying to ruse.."
	scp *.png ruse:/var/www/
	echo "Sleep 60s"
	sleep 60s
done

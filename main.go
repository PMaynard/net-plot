package main

import (
	"flag"
	"io"
	"log"
	"net"
	"strconv"
	"time"

	"github.com/ziutek/rrd"
)

func recvConn(c net.Conn, verbose bool, rrdFile string) {
	buf := make([]byte, 0, 4096)
	tmp := make([]byte, 256)

	// Keep reading until EOF
	for {
		n, err := c.Read(tmp)
		if err != nil {
			if err != io.EOF {
				if verbose {
					log.Println("read error:", err)
				}
			}
			break
		}
		if verbose {
			log.Println("got", n, "bytes.")
		}
		// Smush 256 into buffer.
		buf = append(buf, tmp[:n]...)
	}
	if verbose {
		log.Println("total size:", len(buf))
	}
	temp, err := strconv.ParseFloat(string(buf[5:16]), 64)
	if err != nil {
		log.Println(err)
	}

	if verbose {
		log.Printf("%.6f\n", temp)
	}

	u := rrd.NewUpdater(rrdFile)
	err = u.Update(time.Now(), temp)
	if err != nil {
		log.Panic(err)
	}
	err = c.Close()
	if err != nil {
		log.Println(err)
	}
}

func main() {
	verbosePtr := flag.Bool("verbose", false, "Enable verbose output")
	listenPtr := flag.String("listen", ":12345", "Interface and TCP port to listen on [Default \":12345\"]")
	rrdFilePtr := flag.String("rrd", "./out.rrd", "rrd file to use [Default ./out.rrd]")
	flag.Parse()

	inf, err := rrd.Info(*rrdFilePtr)
	if err != nil {
		log.Panic(err)
	}

	log.Println("Using rrd file:", *rrdFilePtr)
	if *verbosePtr {
		for k, v := range inf {
			log.Printf("%s (%T): %v\n", k, v, v)
		}
	}

	l, err := net.Listen("tcp", *listenPtr)
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("Listening (TCP)", *listenPtr)
	defer l.Close()
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Panic(err)
		}
		go recvConn(conn, *verbosePtr, *rrdFilePtr)
	}
}
